package effective.java.chapter2.builderPattern;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Builder(builderMethodName = "hiddenBuilder")
@Data
public class NutritionFactsWithLombok {

    private final int servingSize;  //required
    private final int servings;     //required
    private final int calories;
    private final int fat;
    private final int sodium;
    private final int carbohydrate;

}
