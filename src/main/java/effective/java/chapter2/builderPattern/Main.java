package effective.java.chapter2.builderPattern;

public class Main {
    public static void main(String[] args) {

        NutritionFacts cocaCola1= new NutritionFacts.Builder(240,8).
                calories(100).
                sodium(35).
                carbohydrate(27).
                build();

        System.out.println(cocaCola1);

        NutritionFactsWithLombok cocaCola2=new NutritionFactsWithLombok.NutritionFactsWithLombokBuilder().
                servingSize(240).
                servings(8).
                calories(100).
                sodium(35).
                carbohydrate(27).
                build();



    }
}
